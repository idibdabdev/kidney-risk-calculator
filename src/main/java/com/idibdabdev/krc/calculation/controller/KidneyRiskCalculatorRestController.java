package com.idibdabdev.krc.calculation.controller;

import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.idibdabdev.krc.calculation.model.donor.Donor;
import com.idibdabdev.krc.calculation.model.donor.calculator.DonorRiskIndex;
import com.idibdabdev.krc.calculation.model.recipient.Recipient;
import com.idibdabdev.krc.calculation.model.recipient.calculator.RecipientRiskCalculator;
import com.idibdabdev.krc.calculation.model.recipient.calculator.RecipientRiskIndex;
import com.idibdabdev.krc.calculation.service.KidneyRiskCalculatorService;
import com.idibdabdev.krc.calculation.service.RecipientRiskIndexService;

/**
 * This interface specifies a list of methods required to be implemented by a REST controller responsible for handling
 * HTTP JSON based requests for the Kidney Risk Calculator application.
 * @author iaind
 *
 */
public interface KidneyRiskCalculatorRestController extends KidneyRiskCalculatorService {

	/**
	 * Retrieves and serializes a response from the {@link RecipientRiskCalculator} using the
	 * {@link RecipientRiskIndexService} for a given {@link Recipient}
	 * @param recipient - the recipient for which the RRI (Recipient Risk Index) shall be calculated.
	 * @return - A {@link RecipientRiskIndex} object containing the score, relative risk category and the
	 * {@link Recipient} used to calculate the score.
	 */
	public ResponseEntity<RecipientRiskIndex> retrieveRRI(Recipient recipient);

	/**
	 * Produces a JSON response for an embedded set of {@link Recipient} details.
	 * @return an HTTP {@link ResponseEntity} containing a {@link Recipient} de-serialised as JSON.
	 * @throws JsonProcessingException - If the example {@link Recipient} cannot be de-serialized , an exception will be
	 * thrown
	 */
	public ResponseEntity<Recipient> exampleRecipient() throws JsonProcessingException;

	/**
	 * Retrieves and serializes a response from the {@link DonorRiskCalculator} using the {@link DonorRiskIndexService}
	 * for a given {@link Donor}
	 * @param donor - the donor for which the DRI (Donor Risk Index) shall be calculated.
	 * @return - A {@link DonorRiskIndex} object containing the score, relative risk category and the {@link Donor} used
	 * to calculate the score.
	 */
	public ResponseEntity<DonorRiskIndex> retrieveDRI(Donor donor);

	/**
	 * Produces a JSON response for an embedded set of {@link Donor} details.
	 * @return an HTTP {@link ResponseEntity} containing a {@link Donor} de-serialised as JSON.
	 * @throws JsonProcessingException - If the example {@link Donor} cannot be de-serialized , an exception will be
	 * thrown
	 */
	public ResponseEntity<Donor> exampleDonor() throws JsonProcessingException;
}
