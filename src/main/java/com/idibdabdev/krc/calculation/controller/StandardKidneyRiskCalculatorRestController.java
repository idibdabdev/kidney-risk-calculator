package com.idibdabdev.krc.calculation.controller;

import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.idibdabdev.krc.calculation.model.donor.Donor;
import com.idibdabdev.krc.calculation.model.donor.calculator.DonorRiskIndex;
import com.idibdabdev.krc.calculation.model.recipient.Recipient;
import com.idibdabdev.krc.calculation.model.recipient.calculator.RecipientRiskIndex;
import com.idibdabdev.krc.calculation.service.DonorRiskIndexService;
import com.idibdabdev.krc.calculation.service.RecipientRiskIndexService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RestController
@RequestMapping(path = "/kidneyRiskCalculator")
public class StandardKidneyRiskCalculatorRestController	 implements KidneyRiskCalculatorRestController {

	@Autowired
	private DonorRiskIndexService donorRiskIndexService;

	@Autowired
	private RecipientRiskIndexService recipientRiskIndexService;

	public RecipientRiskIndex calculateRRI(Recipient recipient) {

		return recipientRiskIndexService.calculateRRI(recipient);
	}

	public DonorRiskIndex calculateDRI(Donor donor) {
		return donorRiskIndexService.calculateDRI(donor);
	}

	@Override
	@Operation(summary = "Calculates the associated risk score and category for a given kidney recipient." , description = "")
	@ApiResponses(value = {
	        @ApiResponse(responseCode = "200", description = "The calculation of the RRI was successful for the set of provided factors.", 
	                content = @Content(array = @ArraySchema(schema = @Schema(implementation = RecipientRiskIndex.class)))) })
	@PostMapping(path = "/recipientRiskIndex/calculate", consumes = "application/json", produces = "application/json")
	public ResponseEntity<RecipientRiskIndex> retrieveRRI(@NotBlank @io.swagger.v3.oas.annotations.parameters.RequestBody @RequestBody Recipient recipient) {
		return ResponseEntity.ok(calculateRRI(recipient));
	}

	@Override
	@Operation(summary = "Calculates the associated risk score and category for a given kidney donor." , description = "")
	@ApiResponses(value = {
	        @ApiResponse(responseCode = "200", description = "The calculation of the DRI was successful for the set of provided factors.", 
	                content = @Content(array = @ArraySchema(schema = @Schema(implementation = DonorRiskIndex.class)))) })
	@PostMapping(path = "/donorRiskIndex/calculate", consumes = "application/json", produces = "application/json")
	public ResponseEntity<DonorRiskIndex> retrieveDRI(@NotBlank @io.swagger.v3.oas.annotations.parameters.RequestBody  @RequestBody Donor donor) {
		return ResponseEntity.ok(calculateDRI(donor));
	}

	@Override
	@Operation(summary = "Provides an example model json for a kidney recipient." , description = "")
	@ApiResponses(value = {
	        @ApiResponse(responseCode = "200", description = "Retrieved an embedded example of risk factors representing kidney recipient", 
	                content = @Content(array = @ArraySchema(schema = @Schema(implementation = Recipient.class)))) })
	@GetMapping(path = "/recipientRiskIndex/example", produces = "application/json")
	public ResponseEntity<Recipient> exampleRecipient() throws JsonProcessingException {
		return ResponseEntity.ok(recipientRiskIndexService.getExampleRecipient());
	}

	@Override
	@Operation(summary = "Provides an example model json for a kidney donor." , description = "")
	@ApiResponses(value = {
	        @ApiResponse(responseCode = "200", description = "Retrieved an embedded example of risk factors representing kidney donor", 
	                content = @Content(array = @ArraySchema(schema = @Schema(implementation = Donor.class)))) })
	@GetMapping(path = "/donorRiskIndex/example", produces = "application/json")
	public ResponseEntity<Donor> exampleDonor() throws JsonProcessingException {
		return ResponseEntity.ok(donorRiskIndexService.getExampleDonor());
	}
}
