package com.idibdabdev.krc.calculation.model.donor.factors;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class DonorFactor extends AbstractFactor {
	@Builder
	public DonorFactor(FactorType factorType, Object factorValue) {
		super(factorType, factorValue);
	}
}
