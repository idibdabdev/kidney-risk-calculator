package com.idibdabdev.krc.calculation.model.donor.sample;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import com.idibdabdev.krc.calculation.model.donor.factors.DonorFactor;
import com.idibdabdev.krc.calculation.model.donor.factors.Factor;
import com.idibdabdev.krc.calculation.model.donor.factors.FactorType;

public class DonorFactorSamples {

	/** Accidental Donors */
	static final Set<Factor> donor1 = Set.of(new DonorFactor(FactorType.DONORAGE, 50),
			new DonorFactor(FactorType.DONOREGFR, 50), new DonorFactor(FactorType.DAYSINHOSPITAL, 14),
			new DonorFactor(FactorType.DONORHEIGHT, 170), new DonorFactor(FactorType.FEMALEDONOR, true),
			new DonorFactor(FactorType.HISTORYOFHYPERTENSION, false), new DonorFactor(FactorType.CMVPSTVEDONOR, false));

	static final Set<Factor> donor2 = Set.of(new DonorFactor(FactorType.DONORAGE, 27),
			new DonorFactor(FactorType.DONOREGFR, 70), new DonorFactor(FactorType.DAYSINHOSPITAL, 14),
			new DonorFactor(FactorType.DONORHEIGHT, 180), new DonorFactor(FactorType.FEMALEDONOR, false),
			new DonorFactor(FactorType.HISTORYOFHYPERTENSION, false), new DonorFactor(FactorType.CMVPSTVEDONOR, false));

	static final Set<Factor> donor3 = Set.of(new DonorFactor(FactorType.DONORAGE, 60),
			new DonorFactor(FactorType.DONOREGFR, 70), new DonorFactor(FactorType.DAYSINHOSPITAL, 14),
			new DonorFactor(FactorType.DONORHEIGHT, 180), new DonorFactor(FactorType.FEMALEDONOR, false),
			new DonorFactor(FactorType.HISTORYOFHYPERTENSION, false), new DonorFactor(FactorType.CMVPSTVEDONOR, false));

	static final Set<Factor> donor4 = Set.of(new DonorFactor(FactorType.DONORAGE, 35),
			new DonorFactor(FactorType.DONOREGFR, 66), new DonorFactor(FactorType.DAYSINHOSPITAL, 14),
			new DonorFactor(FactorType.DONORHEIGHT, 160), new DonorFactor(FactorType.FEMALEDONOR, true),
			new DonorFactor(FactorType.HISTORYOFHYPERTENSION, false), new DonorFactor(FactorType.CMVPSTVEDONOR, false));

	/** Chronic Illness Donors */
	static final Set<Factor> cidonor1 = Set.of(new DonorFactor(FactorType.DONORAGE, 35),
			new DonorFactor(FactorType.DONOREGFR, 70), new DonorFactor(FactorType.DAYSINHOSPITAL, 30),
			new DonorFactor(FactorType.DONORHEIGHT, 160), new DonorFactor(FactorType.FEMALEDONOR, true),
			new DonorFactor(FactorType.HISTORYOFHYPERTENSION, true), new DonorFactor(FactorType.CMVPSTVEDONOR, false));

	static final Set<Factor> cidonor2 = Set.of(new DonorFactor(FactorType.DONORAGE, 50),
			new DonorFactor(FactorType.DONOREGFR, 55), new DonorFactor(FactorType.DAYSINHOSPITAL, 30),
			new DonorFactor(FactorType.DONORHEIGHT, 160), new DonorFactor(FactorType.FEMALEDONOR, true),
			new DonorFactor(FactorType.HISTORYOFHYPERTENSION, true), new DonorFactor(FactorType.CMVPSTVEDONOR, false));

	public static final List<Set<Factor>> donorList = Arrays.asList(donor1, donor2, donor3, donor4, cidonor1, cidonor2);
}
