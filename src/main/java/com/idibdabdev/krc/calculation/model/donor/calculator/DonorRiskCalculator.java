package com.idibdabdev.krc.calculation.model.donor.calculator;

import java.util.Set;

import org.springframework.stereotype.Component;

import com.idibdabdev.krc.calculation.model.donor.Donor;
import com.idibdabdev.krc.calculation.model.donor.factors.Factor;
import com.idibdabdev.krc.calculation.model.donor.factors.FactorType;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Data
@NoArgsConstructor
@Component
public class DonorRiskCalculator {

	public DonorRiskIndex calculateDRI(Donor donor) {

		Double donorAgeRisk = calculateOldDonorRisk(donor.getDonorAge());
		Double donorHeightRisk = calculateDonorHeightRisk(donor.getDonorHeightCM());
		Double donorHypertensionRisk = calculateDonorHyperTensionRisk(donor.isHistoryOfHypertension());
		Double donorGenderRisk = calculateDonorIsFemale(donor.isFemaleDonor());
		Double donorCMVRisk = calculateDonorHasCMVRisk(donor.isCMVPositive());
		Double donorEGFRRisk = calculateDonorEGFRRisk(donor.getDonorEGFR());
		Double donorDaysInHosptial = calculateDonorDaysInHospitalRisk(donor.getDaysInHospital());

		log.debug("Donor Age Risk: " + donorAgeRisk);

		log.debug("Donor Height Risk: " + donorHeightRisk);

		log.debug("Donor Hypertension Risk: " + donorHypertensionRisk);

		log.debug("Donor Gender Risk:" + donorGenderRisk);

		log.debug("Donor CMV Risk :" + donorCMVRisk);

		log.debug("Donor EGFR Risk :" + donorEGFRRisk);

		log.debug("Donor Days In Hospital Risk: " + donorDaysInHosptial);

		return new DonorRiskIndex(Math.exp(donorAgeRisk + donorHeightRisk + donorHypertensionRisk + donorGenderRisk
				+ donorCMVRisk + donorEGFRRisk + donorDaysInHosptial), donor);
	}

	private Double calculateOldDonorRisk(Integer recipientAge) {
		return 0.023 * (recipientAge - 50);
	}

	private Double calculateDonorHeightRisk(Integer donorHeightCentimeters) {
		return -0.152 * donorHeightCentimeters;
	}

	private Double calculateDonorHyperTensionRisk(boolean hasHyperTension) {
		int hyperTensionModifier = (boolean) hasHyperTension ? 1 : 0;
		return 0.149 * (hyperTensionModifier);
	}

	private Double calculateDonorIsFemale(boolean isFemale) {
		int isFemaleValue = (boolean) isFemale ? 1 : 0;
		return -0.184 * (isFemaleValue);
	}

	private Double calculateDonorHasCMVRisk(boolean hasCMV) {
		int hasCMVValue = (boolean) hasCMV ? 1 : 0;
		return 0.190 * (hasCMVValue);
	}

	private Double calculateDonorDaysInHospitalRisk(Integer daysInHospital) {
		int daysInHospitalValue = (int) daysInHospital;
		return 0.015 * (daysInHospitalValue);
	}

	private Double calculateDonorEGFRRisk(Integer donorEgfr) {
		int donorEgfrValue = (int) donorEgfr;
		return -0.023 * ((donorEgfrValue - 90) / 10) * donorEgfrValue;
	}

	public static Factor getDonorFactor(Set<Factor> donorRisks, FactorType donorFactorType) {
		return donorRisks.stream().filter(factor -> factor.getFactorType() == donorFactorType).findFirst().get();
	}
}
