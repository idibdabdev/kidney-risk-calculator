/** */
package com.idibdabdev.krc.calculation.model.recipient.factors;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * This abstract defines the basic properties a factor of the calculation used to assess the Recipient Risk Score in
 * kidney transplanation of a deceased donor. @author iaind
 */
@Data
@AllArgsConstructor
public abstract class AbstractFactor implements Factor {

	private FactorType factorType;

	private Object value;
}
