package com.idibdabdev.krc.calculation.model.recipient.calculator;

import java.util.Set;

import org.springframework.stereotype.Component;

import com.idibdabdev.krc.calculation.model.recipient.Recipient;
import com.idibdabdev.krc.calculation.model.recipient.factors.Factor;
import com.idibdabdev.krc.calculation.model.recipient.factors.FactorType;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
@Data
@NoArgsConstructor
public class RecipientRiskCalculator {

	public RecipientRiskIndex calculateRRI(Recipient kidneyRecipient) {

		Double recipientAgeRisk = calculateYoungAdultRecipientAgeRisk(kidneyRecipient.getRecipientAge());
		Double onDialysisRisk = calculateRecipientOnDialysisAtRegistrationRisk(
				kidneyRecipient.isRecipientOnDialysisAtRegistration());

		Double diabeticRisk = calculateDiabeticRisk(kidneyRecipient.isDiabetic());
		Double waitingOnDialysisRisk = calculateRecipientWaitingOnDialysisAdvantage(
				kidneyRecipient.getDaysWaitingOnDialysis());

		log.debug("Recipient Age Risk: " + recipientAgeRisk);

		log.debug("On Dialysis Risk: " + onDialysisRisk);

		log.debug("Diabetic Risk: " + diabeticRisk);

		log.debug("Waiting Advantage: " + waitingOnDialysisRisk);

		return new RecipientRiskIndex(
				Math.exp(recipientAgeRisk + onDialysisRisk + diabeticRisk + waitingOnDialysisRisk), kidneyRecipient);
	}

	private Double calculateYoungAdultRecipientAgeRisk(Integer recipientAge) {

		int youngAdult = 0;
		int adult = 1;

		if (recipientAge <= 25) {
			youngAdult = 1;
			adult = 0;
		}

		return (0 * (youngAdult - 75)) + (0.016 * ((adult) - 75));
	}

	private Double calculateRecipientOnDialysisAtRegistrationRisk(Boolean isOnDialysis) {
		int dialysisModifier = (boolean) isOnDialysis ? 1 : 0;
		return 0.361 * dialysisModifier;
	}

	private Double calculateRecipientWaitingOnDialysisAdvantage(Integer daysWaitingForTransplant) {

		int dialysisModifier = (int) daysWaitingForTransplant;

		return 0.033 * ((dialysisModifier - 950) / 365.25);
	}

	private Double calculateDiabeticRisk(Boolean isDiabetic) {
		int diabeticModifier = isDiabetic ? 1 : 0;
		return 0.252 * (diabeticModifier);
	}

	public static Factor getRecipientFactor(Set<Factor> recipientRisks, FactorType recipientFactorType) {
		return recipientRisks.stream().filter(factor -> factor.getFactorType() == recipientFactorType).findFirst()
				.get();
	}
}
