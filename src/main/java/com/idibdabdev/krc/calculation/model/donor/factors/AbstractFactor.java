/** */
package com.idibdabdev.krc.calculation.model.donor.factors;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This abstract defines the basic properties a factor of the calculation used to assess the Donor Risk Score in kidney
 * transplanation of a deceased donor. @author iaind
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractFactor implements Factor {

	private FactorType factorType;

	private Object value;
}
