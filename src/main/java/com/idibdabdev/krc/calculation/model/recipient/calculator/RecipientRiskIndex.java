/** */
package com.idibdabdev.krc.calculation.model.recipient.calculator;

import com.idibdabdev.krc.calculation.model.recipient.Recipient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

/**
 * A recipient is then categorised in to one of 4 groups based on the risk score and by pre-determined cut-off values.
 * R1 (lowest risk), R2, R3 and R4 (highest risk).
 *
 * @author iaind
 */
@Log4j2
@Data
@AllArgsConstructor
public class RecipientRiskIndex {

	private String recipientRiskIndexCategory;

	private Double recipientRiskIndexScore;

	private Recipient associatedKidneyRecipient;

	public RecipientRiskIndex(Double recipientRiskIndexScore, Recipient kidneyRecipient) {

		this.recipientRiskIndexScore = recipientRiskIndexScore;
		createRecipientRiskIndex(recipientRiskIndexScore);
		this.associatedKidneyRecipient = kidneyRecipient;
		log.debug("Recipient Risk Score :" + getRecipientRiskIndexScore());
		log.debug("Recipient Risk Index :" + getRecipientRiskIndexCategory());
	}

	private void createRecipientRiskIndex(Double recipientRiskIndexScore) {
		if (RecipientRiskIndexValues.R1_CUTOFF >= recipientRiskIndexScore) {
			recipientRiskIndexCategory = "R1";
		}
		else if (RecipientRiskIndexValues.R1_CUTOFF < recipientRiskIndexScore
				&& RecipientRiskIndexValues.R2_CUTOFF >= recipientRiskIndexScore) {
			recipientRiskIndexCategory = "R2";
		}
		else if (RecipientRiskIndexValues.R2_CUTOFF < recipientRiskIndexScore
				&& RecipientRiskIndexValues.R3_CUTOFF >= recipientRiskIndexScore) {
			recipientRiskIndexCategory = "R2";
		}
		else {
			recipientRiskIndexCategory = "R4";
		}
	}
}
