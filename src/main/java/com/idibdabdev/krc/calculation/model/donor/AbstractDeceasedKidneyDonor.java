package com.idibdabdev.krc.calculation.model.donor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Defines attributes of an default Kidney Donor.
 *
 * @author iaind
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractDeceasedKidneyDonor implements Donor {

	@Getter
	@Setter
	private Integer donorAge;

	@Getter
	@Setter
	private Integer donorHeightCM;

	@Getter
	@Setter
	private boolean historyOfHypertension;

	@Getter
	@Setter
	private boolean femaleDonor;

	@Getter
	@Setter
	private boolean CMVPositive;

	@Getter
	@Setter
	private Integer donorEGFR;

	@Getter
	@Setter
	private Integer daysInHospital;

}
