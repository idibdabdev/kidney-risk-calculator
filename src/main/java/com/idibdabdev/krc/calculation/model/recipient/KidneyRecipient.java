package com.idibdabdev.krc.calculation.model.recipient;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * This class models the neccessary information used to calculate risk and acceptability of a kidney for a given
 * recipient
 *
 * @author iaind
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class KidneyRecipient extends AbstractKidneyRecipient {

	@Builder
	@JsonCreator
	public KidneyRecipient(@JsonProperty("recipientAge") Integer recipientAge,
			@JsonProperty("diabetic") Boolean diabetic,
			@JsonProperty("recipientOnDialysisAtRegistration") Boolean recipientOnDialysisAtRegistration,
			@JsonProperty("daysWaitingOnDialysis") Integer daysWaitingOnDialysis) {
		super(recipientAge, diabetic, recipientOnDialysisAtRegistration, daysWaitingOnDialysis);
	}

	@Builder
	public KidneyRecipient() {
		super();
	}

}
