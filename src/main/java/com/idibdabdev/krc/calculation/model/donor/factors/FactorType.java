package com.idibdabdev.krc.calculation.model.donor.factors;

/**
 * This enum defines the various types of factor types used to define the donor risk score
 *
 * @author iaind
 */
public enum FactorType {
	DONORAGE, DONORHEIGHT, HISTORYOFHYPERTENSION, FEMALEDONOR, CMVPSTVEDONOR, DONOREGFR, DAYSINHOSPITAL
}
