package com.idibdabdev.krc.calculation.model.recipient.calculator;

public class RecipientRiskIndexValues {

	public static final Double R1_CUTOFF = 0.74;

	public static final Double R2_CUTOFF = 0.94;

	public static final Double R3_CUTOFF = 1.20;
}
