package com.idibdabdev.krc.calculation.model.recipient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AbstractKidneyRecipient implements Recipient {

	@Getter
	@Setter
	private Integer recipientAge;

	@Getter
	@Setter
	private boolean diabetic;

	@Getter
	@Setter
	private boolean recipientOnDialysisAtRegistration;

	@Getter
	@Setter
	private Integer daysWaitingOnDialysis;

}
