package com.idibdabdev.krc.calculation.model.recipient.factors;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class RecipientFactor extends AbstractFactor {
	@Builder
	public RecipientFactor(FactorType factorType, Object factorValue) {
		super(factorType, factorValue);
	}
}
