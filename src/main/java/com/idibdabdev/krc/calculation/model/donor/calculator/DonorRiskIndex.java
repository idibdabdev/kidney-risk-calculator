package com.idibdabdev.krc.calculation.model.donor.calculator;

import com.idibdabdev.krc.calculation.model.donor.Donor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

/**
 * A donor is then categorised in to one of 4 groups based on the risk score and by pre-determined cut-off values. R1
 * (lowest risk), R2, R3 and R4 (highest risk).
 *
 * @author iaind
 */
@Log4j2
@Data
@AllArgsConstructor
public class DonorRiskIndex {

	private String donorRiskIndexCategory;

	private Double donorRiskIndexScore;

	private Donor associatedKidneyDonor;

	public DonorRiskIndex(Double recipientRiskIndexScore, Donor donor) {

		this.donorRiskIndexScore = recipientRiskIndexScore;
		createRecipientRiskIndex(recipientRiskIndexScore);
		this.associatedKidneyDonor = donor;
		log.debug("Donor Risk Score :" + getDonorRiskIndexScore());
		log.debug("Donor Risk Index :" + getDonorRiskIndexCategory());
	}

	private void createRecipientRiskIndex(Double recipientRiskIndexScore) {
		if (recipientRiskIndexScore.compareTo(DonorRiskIndexValues.D1_CUTOFF) <= 0) {
			donorRiskIndexCategory = "R1";
		}

		if (recipientRiskIndexScore.compareTo(DonorRiskIndexValues.D1_CUTOFF) > 0
				&& recipientRiskIndexScore.compareTo(DonorRiskIndexValues.D2_CUTOFF) <= 0) {
			donorRiskIndexCategory = "R2";
		}

		if (recipientRiskIndexScore.compareTo(DonorRiskIndexValues.D2_CUTOFF) > 0
				&& recipientRiskIndexScore.compareTo(DonorRiskIndexValues.D3_CUTOFF) <= 0) {
			donorRiskIndexCategory = "R2";
		}
		donorRiskIndexCategory = "R4";
	}

}
