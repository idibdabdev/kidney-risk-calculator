package com.idibdabdev.krc.calculation.model.donor;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * This class models the neccessary information used to calculate risk and acceptability of a kidney for a given donor
 *
 * @author iaind
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class KidneyDonor extends AbstractDeceasedKidneyDonor {

	@Builder
	@JsonCreator
	public KidneyDonor(@JsonProperty("donorAge") Integer donorAge, @JsonProperty("donorHeightCM") Integer donorHeightCM,
			@JsonProperty("historyOfHypertension") boolean historyOfHypertension,
			@JsonProperty("femaleDonor") boolean femaleDonor, @JsonProperty("CMVpositive") boolean CMVPositive,
			@JsonProperty("donorEGFR") Integer donorEGFR, @JsonProperty("daysInHospital") Integer daysInHospital) {
		super(donorAge, donorHeightCM, historyOfHypertension, femaleDonor, CMVPositive, donorEGFR, daysInHospital);
	}

	@Builder
	public KidneyDonor() {
		super();
	}

}
