package com.idibdabdev.krc.calculation.model.donor;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = KidneyDonor.class)
public interface Donor {

	public Integer getDonorAge();

	public Integer getDonorHeightCM();

	public boolean isHistoryOfHypertension();

	public boolean isFemaleDonor();

	public boolean isCMVPositive();

	public Integer getDonorEGFR();

	public Integer getDaysInHospital();
}
