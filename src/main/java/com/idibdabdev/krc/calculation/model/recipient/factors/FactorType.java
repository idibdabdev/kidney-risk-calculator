package com.idibdabdev.krc.calculation.model.recipient.factors;

/**
 * This enum defines the various types of factor types used to define the donor risk score
 *
 * @author iaind
 */
public enum FactorType {
	RECIPIENTAGE, RECIEPIENTONDIALYSISATREGISTRAION, WAITINGTIMEONDIALYSIS, DIABETICRECIPIENT
}
