package com.idibdabdev.krc.calculation.model.donor.calculator;

public class DonorRiskIndexValues {
	public static Double D1_CUTOFF = 0.79;

	public static Double D2_CUTOFF = 1.12;

	public static Double D3_CUTOFF = 1.50;
}
