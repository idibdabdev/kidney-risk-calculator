/** */
package com.idibdabdev.krc.calculation.model.donor.factors;

/**
 * This interface defines a factor of the calculation used to assess the Donor Risk Score in kidney transplanation of a
 * deceased donor.
 *
 * @author iaind
 */
public interface Factor {
	public FactorType getFactorType();

	public Object getValue();
}
