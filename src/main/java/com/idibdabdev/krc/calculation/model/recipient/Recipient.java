package com.idibdabdev.krc.calculation.model.recipient;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = KidneyRecipient.class)
public interface Recipient {

	public Integer getRecipientAge();

	public boolean isDiabetic();

	public boolean isRecipientOnDialysisAtRegistration();

	public Integer getDaysWaitingOnDialysis();

}
