/** */
package com.idibdabdev.krc.calculation.model.recipient.factors;

/**
 * This interface defines a factor of the calculation used to assess the Recipient Risk Score in kidney transplanation
 * of a deceased donor.
 *
 * @author iaind
 */
public interface Factor {

	public FactorType getFactorType();

	public void setFactorType(FactorType factorTyoe);

	public Object getValue();

	public void setValue(Object value);
}
