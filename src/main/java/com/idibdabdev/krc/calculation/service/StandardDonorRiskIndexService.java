package com.idibdabdev.krc.calculation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idibdabdev.krc.calculation.model.donor.Donor;
import com.idibdabdev.krc.calculation.model.donor.calculator.DonorRiskCalculator;
import com.idibdabdev.krc.calculation.model.donor.calculator.DonorRiskIndex;
import com.idibdabdev.krc.calculation.util.KidneyRiskCalculatorStubValues;

@Service
public class StandardDonorRiskIndexService implements DonorRiskIndexService {
	@Autowired
	private DonorRiskCalculator donorRiskCalculator;

	@Override
	public Donor getExampleDonor() {
		return KidneyRiskCalculatorStubValues.EXAMPLE_DONOR;
	}

	@Override
	public DonorRiskIndex calculateDRI(Donor donor) {
		return donorRiskCalculator.calculateDRI(donor);
	}

}
