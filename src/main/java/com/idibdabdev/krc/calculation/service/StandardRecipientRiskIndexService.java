package com.idibdabdev.krc.calculation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idibdabdev.krc.calculation.model.recipient.Recipient;
import com.idibdabdev.krc.calculation.model.recipient.calculator.RecipientRiskCalculator;
import com.idibdabdev.krc.calculation.model.recipient.calculator.RecipientRiskIndex;
import com.idibdabdev.krc.calculation.util.KidneyRiskCalculatorStubValues;

@Service
public class StandardRecipientRiskIndexService implements RecipientRiskIndexService {

	@Autowired
	private RecipientRiskCalculator recipientRiskCalculator;

	@Override
	public Recipient getExampleRecipient() {
		return KidneyRiskCalculatorStubValues.EXAMPLE_RECIPIENT;
	}

	@Override
	public RecipientRiskIndex calculateRRI(Recipient kidneyRecipient) {
		return recipientRiskCalculator.calculateRRI(kidneyRecipient);
	}

}
