package com.idibdabdev.krc.calculation.service;

import com.idibdabdev.krc.calculation.model.donor.Donor;
import com.idibdabdev.krc.calculation.model.donor.calculator.DonorRiskIndex;
import com.idibdabdev.krc.calculation.model.recipient.Recipient;
import com.idibdabdev.krc.calculation.model.recipient.calculator.RecipientRiskIndex;

public interface KidneyRiskCalculatorService {
	/**
	 * Invokes the necessary services used to produce a RRI (Recipient Risk Index) calculation for a given
	 * {@link Recipient}
	 * @param recipient - the recipient for which the RRI (Recipient Risk Index) shall be calculated.
	 * @return
	 */
	public RecipientRiskIndex calculateRRI(Recipient recipient);

	/**
	 * Invokes the necessary services used to produce a DRI (Donor Risk Index) calculation for a given {@link Donor}
	 * @param donor - the donor for which the DRI (Donor Risk Index) shall be calculated.
	 * @return
	 */
	public DonorRiskIndex calculateDRI(Donor donor);
}
