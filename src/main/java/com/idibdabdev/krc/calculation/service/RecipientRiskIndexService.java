package com.idibdabdev.krc.calculation.service;

import com.idibdabdev.krc.calculation.model.recipient.Recipient;
import com.idibdabdev.krc.calculation.model.recipient.calculator.RecipientRiskCalculator;
import com.idibdabdev.krc.calculation.model.recipient.calculator.RecipientRiskIndex;

/**
 * This interface specifies the require methods for implementation by a {@link RecipientRiskIndexService}
 * @author iaind
 */
public interface RecipientRiskIndexService {
	/**
	 * Returns the specifically embedded {@link Recipient} example for the given implementation of the
	 * {@link RecipientRiskIndexService}.
	 * @return a example {@link Recipient}
	 */
	public Recipient getExampleRecipient();

	/**
	 * Performs the calculation of RRI using the {@link RecipientRiskCalculator}
	 * @param kidneyRecipient - the recipient for which the RRI (Recipient Risk Index) shall be calculated.
	 * @return A {@link RecipientRiskIndex} object containing the score, relative risk category and the Recipient used
	 * to calculate the score.
	 */
	public RecipientRiskIndex calculateRRI(Recipient kidneyRecipient);
}
