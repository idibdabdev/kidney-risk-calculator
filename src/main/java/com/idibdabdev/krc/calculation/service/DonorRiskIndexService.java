package com.idibdabdev.krc.calculation.service;

import com.idibdabdev.krc.calculation.model.donor.Donor;
import com.idibdabdev.krc.calculation.model.donor.calculator.DonorRiskIndex;
import com.idibdabdev.krc.calculation.model.recipient.calculator.RecipientRiskCalculator;
import com.idibdabdev.krc.calculation.model.recipient.calculator.RecipientRiskIndex;

/**
 * This interface specifies the require methods for implementation by a {@link DonorRiskIndexService}
 * @author iaind
 */
public interface DonorRiskIndexService {
	/**
	 * Returns the specifically embedded {@link Donor} example for the given implementation of the
	 * {@link DonorRiskIndexService}.
	 * @return a example {@link Donor}
	 */
	public Donor getExampleDonor();

	/**
	 * Performs the calculation of DRI using the {@link DonorRiskCalculator}
	 * @param kidneyRecipient - the recipient for which the DRI (Donor Risk Index) shall be calculated.
	 * @return A {@link DonorRiskIndex} object containing the score, relative risk category and the Recipient used to
	 * calculate the score.
	 */
	public DonorRiskIndex calculateDRI(Donor donor);
}
