package com.idibdabdev.krc.calculation.util;

import java.util.Set;

import com.idibdabdev.krc.calculation.model.donor.Donor;
import com.idibdabdev.krc.calculation.model.donor.KidneyDonor;
import com.idibdabdev.krc.calculation.model.donor.factors.DonorFactor;
import com.idibdabdev.krc.calculation.model.recipient.KidneyRecipient;
import com.idibdabdev.krc.calculation.model.recipient.Recipient;
import com.idibdabdev.krc.calculation.model.recipient.factors.RecipientFactor;

public class KidneyRiskCalculatorStubValues {
	public static Set<com.idibdabdev.krc.calculation.model.recipient.factors.Factor> EXAMPLE_RECIPIENT_FACTORS = Set.of(
			new RecipientFactor(com.idibdabdev.krc.calculation.model.recipient.factors.FactorType.RECIPIENTAGE, 50),
			new RecipientFactor(com.idibdabdev.krc.calculation.model.recipient.factors.FactorType.DIABETICRECIPIENT,
					false),
			new RecipientFactor(
					com.idibdabdev.krc.calculation.model.recipient.factors.FactorType.RECIEPIENTONDIALYSISATREGISTRAION,
					false),
			new RecipientFactor(com.idibdabdev.krc.calculation.model.recipient.factors.FactorType.WAITINGTIMEONDIALYSIS,
					265));

	public static Recipient EXAMPLE_RECIPIENT = new KidneyRecipient(50, false, false, 265);

	public static Set<com.idibdabdev.krc.calculation.model.donor.factors.Factor> EXAMPLE_DONOR_FACTORS = Set.of(
			new DonorFactor(com.idibdabdev.krc.calculation.model.donor.factors.FactorType.HISTORYOFHYPERTENSION, true),
			new DonorFactor(com.idibdabdev.krc.calculation.model.donor.factors.FactorType.FEMALEDONOR, true),
			new DonorFactor(com.idibdabdev.krc.calculation.model.donor.factors.FactorType.DONORHEIGHT, 175),
			new DonorFactor(com.idibdabdev.krc.calculation.model.donor.factors.FactorType.DONOREGFR, 50),
			new DonorFactor(com.idibdabdev.krc.calculation.model.donor.factors.FactorType.DONORAGE, 65),
			new DonorFactor(com.idibdabdev.krc.calculation.model.donor.factors.FactorType.DAYSINHOSPITAL, 100),
			new DonorFactor(com.idibdabdev.krc.calculation.model.donor.factors.FactorType.CMVPSTVEDONOR, true));

	public static Donor EXAMPLE_DONOR = new KidneyDonor(65, 175, true, true, true, 50, 100);

}
