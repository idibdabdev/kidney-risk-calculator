package com.idibdabdev.krc.api.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.idibdabdev.krc.api.repository.model.DonorDocument;

public interface DonorRepository extends ElasticsearchRepository<DonorDocument, String> {
}
