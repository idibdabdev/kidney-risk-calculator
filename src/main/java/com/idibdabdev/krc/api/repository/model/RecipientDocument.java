package com.idibdabdev.krc.api.repository.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Document(indexName = "krc_recipient", createIndex = true)
@AllArgsConstructor
@NoArgsConstructor
public class RecipientDocument {
	@Id
	private String id;

	@Getter
	@Setter
	@Field(type = FieldType.Text)
	private String name;

	@Getter
	@Setter
	@Field(type = FieldType.Long)
	private Long chiNumber;

	@Getter
	@Setter
	@Field(type = FieldType.Integer)
	private Integer age;

	@Getter
	@Setter
	@Field(type = FieldType.Boolean)
	private Boolean isDiabetic;

	@Getter
	@Setter
	@Field(type = FieldType.Boolean)
	private Boolean onDialysisAtRegistration;

	@Getter
	@Setter
	@Field(type = FieldType.Integer)
	private Integer daysWaitingOnDialysis;
}
