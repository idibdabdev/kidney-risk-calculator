package com.idibdabdev.krc.api.repository.db.elasticsearch.service;

import java.io.IOException;

import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;

import com.idibdabdev.krc.api.repository.model.DonorDocument;
import com.idibdabdev.krc.api.repository.model.RecipientDocument;

public interface ElasticsearchService {
	public IndexResponse save(DonorDocument document) throws IOException;

	public IndexResponse save(RecipientDocument document) throws IOException;
	
	public DeleteResponse delete(RecipientDocument document) throws IOException;
	
	public DeleteResponse delete(DonorDocument document) throws IOException;
	
	public AcknowledgedResponse deleteAll(String index) throws IOException;
	
	public GetResponse findById(String personType, String id) throws IOException;

	public GetResponse findByName(String personType, String name) throws IOException;
}