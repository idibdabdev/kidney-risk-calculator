package com.idibdabdev.krc.api.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.idibdabdev.krc.api.repository.model.RecipientDocument;

public interface RecipientRepository extends ElasticsearchRepository<RecipientDocument, String> {
}
