package com.idibdabdev.krc.api.repository;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@Component
public class JsonMapper {

	private ObjectMapper mapper;

}
