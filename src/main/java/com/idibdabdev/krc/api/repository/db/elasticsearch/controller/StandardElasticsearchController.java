package com.idibdabdev.krc.api.repository.db.elasticsearch.controller;

import java.io.IOException;

import javax.validation.constraints.NotNull;

import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.rest.RestStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.idibdabdev.krc.api.repository.db.elasticsearch.service.ElasticsearchService;
import com.idibdabdev.krc.api.repository.model.DonorDocument;
import com.idibdabdev.krc.api.repository.model.RecipientDocument;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("/repository")
@AllArgsConstructor
public class StandardElasticsearchController {

	@Autowired
	private ElasticsearchService elasticsearchService;

	@Operation(summary = "Adds a DonorDocument to a specified donor-specific index", description = "")
	@ApiResponses(value = {
			@ApiResponse(description = "See https://www.javadoc.io/doc/org.elasticsearch/elasticsearch/latest/org/elasticsearch/rest/RestStatus.html .", content = @Content(array = @ArraySchema(schema = @Schema(implementation = RestStatus.class)))) })
	@PostMapping(path = "/donor/add", consumes = "application/json", produces = "application/json")
	public ResponseEntity<RestStatus> save(@NotNull @RequestBody DonorDocument donor) throws IOException {

		IndexResponse response = elasticsearchService.save(donor);

		return ResponseEntity.ok(response.status());
	}

	@PostMapping(path = "/recipient/add", consumes = "application/json", produces = "application/json")
	@Operation(summary = "Adds a RecipientDocument to a specified donor-specific index", description = "")
	@ApiResponses(value = {
			@ApiResponse(description = "See https://www.javadoc.io/doc/org.elasticsearch/elasticsearch/latest/org/elasticsearch/rest/RestStatus.html .", content = @Content(array = @ArraySchema(schema = @Schema(implementation = RestStatus.class)))) })
	public ResponseEntity<RestStatus> save(@NotNull @RequestBody RecipientDocument recipient) throws IOException {

		IndexResponse response = elasticsearchService.save(recipient);

		return ResponseEntity.ok(response.status());
	}

	@PostMapping(path = "/recipient/delete", consumes = "application/json", produces = "application/json")
	@Operation(summary = "Deletes a RecipientDocument to a specified donor-specific index", description = "")
	@ApiResponses(value = {
			@ApiResponse(description = "See https://www.javadoc.io/doc/org.elasticsearch/elasticsearch/latest/org/elasticsearch/rest/RestStatus.html .", content = @Content(array = @ArraySchema(schema = @Schema(implementation = RestStatus.class)))) })
	public ResponseEntity<RestStatus> delete(@NotNull @RequestBody RecipientDocument recipient) throws IOException {

		DeleteResponse response = elasticsearchService.delete(recipient);

		return ResponseEntity.ok(response.status());
	}

	@PostMapping(path = "/donor/delete", consumes = "application/json", produces = "application/json")
	@Operation(summary = "Deletes a DonorDocument to a specified donor-specific index", description = "")
	@ApiResponses(value = {
			@ApiResponse(description = "See https://www.javadoc.io/doc/org.elasticsearch/elasticsearch/latest/org/elasticsearch/rest/RestStatus.html .", content = @Content(array = @ArraySchema(schema = @Schema(implementation = RestStatus.class)))) })
	public ResponseEntity<RestStatus> delete(@NotNull @RequestBody DonorDocument donor) throws IOException {

		DeleteResponse response = elasticsearchService.delete(donor);

		return ResponseEntity.ok(response.status());
	}

	@PostMapping(path = "/{index}/deleteAll", produces = "application/json")
	@Operation(summary = "Deletes all documents in a specified index", description = "")
	@ApiResponses(value = {
			@ApiResponse(description = "True if the request was acknowledged by the cluster , false otherwise.", content = @Content(array = @ArraySchema(schema = @Schema(implementation = Boolean.class)))) })
	public ResponseEntity<Boolean> delete(@NotNull @PathVariable("index") String index) throws IOException {

		AcknowledgedResponse response = elasticsearchService.deleteAll(index);

		return ResponseEntity.ok(response.isAcknowledged());
	}

	@GetMapping(path = "/{personType}/search/id/{id}", produces = "application/json")
	@Operation(summary = "Searches a specified index for a particular id", description = "")
	public ResponseEntity<String> findById(@NotNull @PathVariable("personType") String personType,
			@NotNull @Parameter @PathVariable("id") String id) throws IOException {

		GetResponse response = elasticsearchService.findById(personType, id);

		return ResponseEntity.ok(response.getSourceAsString());
	}

	@GetMapping(path = "/{personType}/search/name/{name}", produces = "application/json")
	@Operation(summary = "Searches a specified index for a particular name", description = "")
	public ResponseEntity<String> findByName(@NotNull @Parameter @PathVariable("personType") String personType,
			@NotNull @Parameter @PathVariable("name") String name) throws IOException {

		GetResponse response = elasticsearchService.findByName(personType, name);

		return ResponseEntity.ok(response.getSourceAsString());
	}

	@Operation(summary = "Provides an example model json for a kidney recipient stored in elasticsearch.", description = "")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Retrieved an embedded example of a recipient document", content = @Content(array = @ArraySchema(schema = @Schema(implementation = RecipientDocument.class)))) })
	@GetMapping(path = "/recipient/example", produces = "application/json")
	public ResponseEntity<RecipientDocument> exampleRecipientDocument() throws JsonProcessingException {
		return ResponseEntity.ok(new RecipientDocument());
	}

	@Operation(summary = "Provides an example model json for a kidney donor document stored in elasticsearch.", description = "")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Retrieved an embedded example of donor document", content = @Content(array = @ArraySchema(schema = @Schema(implementation = DonorDocument.class)))) })
	@GetMapping(path = "/donor/example", produces = "application/json")
	public ResponseEntity<DonorDocument> exampleDonorDocument() throws JsonProcessingException {
		return ResponseEntity.ok(new DonorDocument());
	}

}