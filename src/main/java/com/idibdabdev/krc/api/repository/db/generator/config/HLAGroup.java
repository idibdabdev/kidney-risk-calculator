package com.idibdabdev.krc.api.repository.db.generator.config;

public enum HLAGroup {
 PERFECTMATCH("000"),GOODMATCH("100-010-110-200-210-001-101-201"),SATISFACTORYMATCH("020-120-220-011-111-211"),POORMATCH("021-121-221-002-102-202-012-112-212-022-122-222");
 
 private String groups;
 HLAGroup(String groups) {
     this.groups=groups;
 }

 public String getGroups() {
     return groups;
 }
}
