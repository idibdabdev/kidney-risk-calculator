package com.idibdabdev.krc.api.repository.db.generator;

import java.util.List;
import java.util.Random;

import org.apache.commons.math3.distribution.EnumeratedDistribution;
import org.apache.commons.math3.random.RandomGeneratorFactory;
import org.apache.commons.math3.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.idibdabdev.krc.api.repository.db.generator.config.DataGeneratorConfiguration;
import com.idibdabdev.krc.calculation.model.donor.Donor;
import com.idibdabdev.krc.calculation.model.donor.KidneyDonor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

@Data
@Log4j2
@Component
@AllArgsConstructor
public class DonorGenerator {

	@Autowired
	private DataGeneratorConfiguration generatorConfig;

	/**
	 * https://www.statista.com/statistics/520163/organ-donation-population-by-gender-united-kingdom-uk/
	 * https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6314572/
	 */
	private List<Pair<String, Double>> genderDonationDistribution;;

	/*
	 * private Donor generate() {
	 * 
	 * String hlaType = generatorConfig.getHlaDistribution().sample();
	 * 
	 * String gender = generatorConfig.getGenderDistribution().sample();
	 * 
	 * double donorHeightSelection = 0.0; switch (gender) { case "Male": // half the number of male donors
	 * donorHeightSelection = generatorConfig.getMaleDonorHeightDistribution().sample(); break; case "Female": // half
	 * the number of female donors donorHeightSelection = generatorConfig.getFemaleDonorHeightDistribution().sample();
	 * break; }
	 * 
	 * // TODO find normal donor age // TODO find history of hypertension on average in uk // TODO find average EGFR for
	 * deceased donors // TODO investigate days spent in hosptial before death of DBD and DCD in ukv return new
	 * KidneyDonor(donorAge, donorHeightSelection, historyOfHypertension, gender, CMVPositive, donorEGFR,
	 * daysInHospital); }
	 */
}
