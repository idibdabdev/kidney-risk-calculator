package com.idibdabdev.krc.api.repository.db.elasticsearch.service;

import java.io.IOException;

import javax.validation.constraints.NotNull;

import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idibdabdev.krc.api.repository.JsonMapper;
import com.idibdabdev.krc.api.repository.db.elasticsearch.config.ElasticsearchConfig;
import com.idibdabdev.krc.api.repository.model.DonorDocument;
import com.idibdabdev.krc.api.repository.model.RecipientDocument;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

@Data
@Log4j2
@Service
@AllArgsConstructor
public class StandardElasticsearchService implements ElasticsearchService {
	@Autowired
	private ElasticsearchConfig config;

	@Autowired
	private JsonMapper mapper;

	private static final String DONOR_INDEX = "krc_donor";

	private static final String RECIPIENT_INDEX = "krc_recipient";

	@Override
	public IndexResponse save(@NotNull DonorDocument document) throws IOException {
		IndexRequest request = new IndexRequest(DONOR_INDEX);

		String json = mapper.getMapper().writeValueAsString(document);
		log.debug("JSON Produced: \n" + json);
		request.source(json, XContentType.JSON);
		return config.client().index(request, RequestOptions.DEFAULT);

	}

	@Override
	public IndexResponse save(@NotNull RecipientDocument document) throws IOException {
		IndexRequest request = new IndexRequest(RECIPIENT_INDEX);

		String json = mapper.getMapper().writeValueAsString(document);
		log.debug("JSON Produced: \n" + json);
		request.source(json, XContentType.JSON);
		return config.client().index(request, RequestOptions.DEFAULT);
	}

	@Override
	public GetResponse findById(@NotNull String personType, @NotNull String id) throws IOException {
		GetRequest getRequest = new GetRequest(determinePersonIndex(personType), id);

		return config.client().get(getRequest, RequestOptions.DEFAULT);
	}

	@Override
	public GetResponse findByName(@NotNull String personType, @NotNull String name) throws IOException {
		GetRequest getRequest = new GetRequest(determinePersonIndex(personType), name);

		return config.client().get(getRequest, RequestOptions.DEFAULT);

	}

	private String determinePersonIndex(@NotNull String personType) {
		String selectedIndex = null;
		switch (personType) {
		case "recipient":
			selectedIndex = RECIPIENT_INDEX;
			break;
		case "donor":
			selectedIndex = DONOR_INDEX;
			break;
		}
		return selectedIndex;
	}

	@Override
	public DeleteResponse delete(RecipientDocument document) throws IOException {
		DeleteRequest deleteDocumentRequest = new DeleteRequest(RECIPIENT_INDEX, document.getId());

		return config.client().delete(deleteDocumentRequest, RequestOptions.DEFAULT);
	}

	@Override
	public DeleteResponse delete(DonorDocument document) throws IOException {
		DeleteRequest deleteDocumentRequest = new DeleteRequest(DONOR_INDEX, document.getId());

		return config.client().delete(deleteDocumentRequest, RequestOptions.DEFAULT);
	}

	@Override
	public AcknowledgedResponse deleteAll(String index) throws IOException {
		DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest(index);
		return config.client().indices().delete(deleteIndexRequest, RequestOptions.DEFAULT);
	}

}
