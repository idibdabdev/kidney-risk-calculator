package com.idibdabdev.krc.api.repository.db.generator.config;

import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.math3.distribution.EnumeratedDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.random.RandomGeneratorFactory;
import org.apache.commons.math3.util.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.idibdabdev.krc.api.repository.db.generator.DonorGenerator;
import com.idibdabdev.krc.api.repository.db.generator.RecipientGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

/**
 * Data Generator configuration , used to store configuration values for the generation of donor and recipient
 * population data for the {@link DonorGenerator} and {@link RecipientGenerator}. See:
 * <p>
 * https://nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/16778/nhsbt-kidney-transplantation-annual-report-2018-19.pdf
 * - transplant statistics
 * </p>
 * <p>
 * https://nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/19192/section-5-kidney-activity.pdf - transplant
 * statistics
 * </p>
 * <p>
 * https://nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/14652/blc7071.pdf - estimate on CMV positive prevalence
 * </p>
 * <p>
 * https://www.usablestats.com/lessons/normal - describes sd for males and female height in inches
 * </p>
 * <p>
 * https://nhsbtdbe.blob.core.windows.net/umbraco-assets-corp/16878/annual-pda-report-2018-19.pdf - potential donor
 * statistics
 * </p>
 * <p>
 * https://www.statista.com/statistics/520163/organ-donation-population-by-gender-united-kingdom-uk/
 * </p>
 * <p>
 * https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6314572/
 * </p>
 * For Statistical Details.
 * @author iaind
 */
@Data
@Log4j2
@Configuration
@AllArgsConstructor
public class DataGeneratorConfiguration {

	// in 2018-Sep 2019 there was 1481 adult transplants performed , so lets use that as the number
	// of
	// available kidneys.

	/** Assumption is made hieght will be of a Normal distribution format for the UK */
	@Value("{data.generator.uk.aspect.cmv.postive.pct:50}")
	private Double cmvPositivePct;

	@Value("{data.generator.uk.male.height.average.cm:175.3}")
	private Double ukMaleHeight;

	@Value("{data.generator.uk.male.donation.pct:46.5}")
	private Double ukMaleDonationPct;

	@Value("{data.generator.uk.female.height.standard.deviation:10.16}")
	private Double ukMaleHeightStandardDeviation;

	@Value("{data.generator.uk.female.height.average.cm:161.6}")
	private Double ukFemaleHeight;

	@Value("{data.generator.uk.female.height.standard.deviation:8.89}")
	private Double ukFemaleHeightStandardDeviation;

	@Value("{data.generator.uk.male.donation.pct:53.5}")
	private Double ukFemaleDonationPct;

	@Value("{data.generator.hla.risk.configuration:000,0.1|100-010-110-200-210-001-101-201, 0.32|020-120-220-011-111-211, 0.41|021-121-221-002-102-202-012-112-212-022-122-222, 0.22}")
	private String HLARiskConfiguration;

	/** Deceased Donor HLA Mistmatch performed transplants , based on Table 5.15 and Table 5.14 */
	private List<Pair<String, Double>> HLAMismatchAssociatedRisk;

	private NormalDistribution maleDonorHeightDistribution;

	private NormalDistribution femaleDonorHeightDistribution;

	/**
	 * https://www.statista.com/statistics/520163/organ-donation-population-by-gender-united-kingdom-uk/
	 * https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6314572/
	 */
	private List<Pair<String, Double>> genderDonationDistribution;

	EnumeratedDistribution<String> hlaDistribution;

	EnumeratedDistribution<String> genderDistribution;

	EnumeratedDistribution<String> cmvDistribution;

	@PostConstruct
	private void performDistributionConstruction() {
		generateGenderDonationDistribution();
		generateHeightDistribtuions();
		generateHLADistribution();
		generateCMVPositiveDistribution();
	}

	private void generateCMVPositiveDistribution() {
		log.info("Generating CMV prevalence Risks");

		double cmvNegativePct = 100 - cmvPositivePct;

		List<Pair<String, Double>> cmvList = List.of(new Pair<String, Double>("CMV+", cmvPositivePct),
				new Pair<String, Double>("CMV-", cmvNegativePct));
		new EnumeratedDistribution<String>(RandomGeneratorFactory.createRandomGenerator(new Random()), cmvList);
	}

	private void generateHLADistribution() {
		log.info("Generating HLA Risks");
		Set<String> HLARiskSet = Set.of(HLARiskConfiguration.split("|"));

		for (String pair : HLARiskSet) {
			String[] hlaRiskPair = pair.split(",");
			HLAMismatchAssociatedRisk
					.add(new Pair<String, Double>(hlaRiskPair[0].replace("-", ","), Double.valueOf(hlaRiskPair[1])));
		}
		log.info("Generating HLA Distribution");

		hlaDistribution = new EnumeratedDistribution<String>(RandomGeneratorFactory.createRandomGenerator(new Random()),
				this.getHLAMismatchAssociatedRisk());
	}

	private void generateGenderDonationDistribution() {
		log.info("Generating Gender Based Donation");

		genderDonationDistribution = List.of(new Pair<>("Male", ukMaleDonationPct),
				new Pair<>("Female", ukFemaleDonationPct));

		log.info("Generating Gender Donation Distribution");

		genderDistribution = new EnumeratedDistribution<String>(
				RandomGeneratorFactory.createRandomGenerator(new Random()), this.getGenderDonationDistribution());

	}

	private void generateHeightDistribtuions() {
		log.info("Generating Male Height Distribution");

		maleDonorHeightDistribution = new NormalDistribution(ukMaleHeight, ukMaleHeightStandardDeviation);
		log.info("Generating Female Height Distribution");

		femaleDonorHeightDistribution = new NormalDistribution(ukFemaleHeight, ukFemaleHeightStandardDeviation);
	}
}
