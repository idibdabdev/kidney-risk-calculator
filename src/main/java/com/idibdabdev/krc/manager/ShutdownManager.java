package com.idibdabdev.krc.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ShutdownManager {

	@Autowired
	private ApplicationContext context;

	public void initiateShutdown(int returnCode) {
		((ConfigurableApplicationContext) context).close();

		SpringApplication.exit(context, () -> returnCode);
	}
}
