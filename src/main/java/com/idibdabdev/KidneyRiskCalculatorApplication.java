package com.idibdabdev;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.FilterType;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

@Log4j2
@SpringBootApplication
// @EnableReactiveMongoRepositories
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.idibdabdev",
excludeFilters = {
		@Filter(type = FilterType.ASSIGNABLE_TYPE, classes = CalculatorRunner.class)/*,
		@ComponentScan.Filter(type = FilterType.REGEX, pattern = "com.idibdabdev.krc.api.repository.*") */})
@Data
public class KidneyRiskCalculatorApplication { // extends AbstractReactiveMongoConfiguration {

	public static void main(String[] args) {

		SpringApplication.run(KidneyRiskCalculatorApplication.class, args);

	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {

			/*
			 * log.info("Let's inspect the beans provided by Spring Boot:");
			 * 
			 * String[] beanNames = ctx.getBeanDefinitionNames(); Arrays.sort(beanNames); for (String beanName :
			 * beanNames) { log.info(beanName); }
			 */
		};
	}

	@Bean
	public OpenAPI krcApi(@Value("${description}") String appDesciption,
			@Value("${version}") String appVersion) {

		return new OpenAPI()
				.info(new Info().title("Kidney Risk Calculator API").version(appVersion).description(appDesciption)
						.termsOfService("http://swagger.io/terms/").license(new License().name("Apache 2.0").url("https://www.apache.org/licenses/LICENSE-2.0"))
						.contact(new Contact().email("iain.dobbin@hotmail.co.uk").name("Iain Dobbin")
								.url("https://bitbucket.org/idibdabdev/kidney-risk-calculator/")))
				;

	}

	/*
	 * @Bean public MongoClient mongoClient() { return MongoClients.create(); }
	 */

	/*
	 * @Override protected String getDatabaseName() { return null; }
	 */
}
