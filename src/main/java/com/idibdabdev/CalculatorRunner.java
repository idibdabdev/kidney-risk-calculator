package com.idibdabdev;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.event.EventListener;

import com.idibdabdev.krc.calculation.model.donor.calculator.DonorRiskCalculator;
import com.idibdabdev.krc.calculation.model.recipient.KidneyRecipient;
import com.idibdabdev.krc.calculation.model.recipient.calculator.RecipientRiskCalculator;
import com.idibdabdev.krc.manager.ShutdownManager;

import lombok.Data;
import lombok.extern.log4j.Log4j2;

@Log4j2
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.idibdabdev.krc.calculation",
		"com.idibdabdev.krc.manager" }, excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = "com.idibdabdev.krc.api.repository.*"))
@Data
public class CalculatorRunner {

	@Autowired
	private ApplicationContext context;

	@Autowired
	private RecipientRiskCalculator recipientCalculator;

	@Autowired
	private DonorRiskCalculator donorCalculator;

	@Autowired
	private ShutdownManager shutdownManager;

	public static void main(String[] args) {
		SpringApplication.run(CalculatorRunner.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void runCaclulator() {

		/*
		 * List<Set<com.idibdabdev.krc.calculation.model.donor.factors.Factor>> donors = DonorFactorSamples.donorList;
		 * 
		 * Set<Factor> iainDobbin2020 = Set.of(new RecipientFactor(FactorType.RECIPIENTAGE, 27), new
		 * RecipientFactor(FactorType.DIABETICRECIPIENT, false), new
		 * RecipientFactor(FactorType.RECIEPIENTONDIALYSISATREGISTRAION, false), new
		 * RecipientFactor(FactorType.WAITINGTIMEONDIALYSIS, 130));
		 * 
		 * Set<Factor> iainDobbin2040 = Set.of(new RecipientFactor(FactorType.RECIPIENTAGE, 75), new
		 * RecipientFactor(FactorType.DIABETICRECIPIENT, false), new
		 * RecipientFactor(FactorType.RECIEPIENTONDIALYSISATREGISTRAION, true), new
		 * RecipientFactor(FactorType.WAITINGTIMEONDIALYSIS, 5000));
		 */
		log.info("Iain Dobbin In 2020 Has these stats");
		recipientCalculator.calculateRRI(new KidneyRecipient(27, false, false, 130));

		log.info("Iain Dobbin In 2040 Has these stats");
		recipientCalculator.calculateRRI(new KidneyRecipient(75, false, true, 5000));
		// log.info("KRC Calculator run complete.Shutting Down...");
		// TODO fix application shutdown to be graceful
		// shutdownManager.initiateShutdown(0);
	}
}
